# WebMiner - JobsDB.com

A web scraper which scrapes job info from Hong Kong's JobsDB.com, then generate a word cloud using TFIDF. 

### Usage
```
Ordinary mode:
$pythonw jobsdbminer.py <Job> -p <result page count> 
Junk Boat mode:
$pythonw jobsdbminer.py <Job> -p <result page count> -j
```

For more details, let's have a look at https://www.codeastar.com/hong-kong-python-word-cloud-job-seekers/